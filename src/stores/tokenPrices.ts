import { acceptHMRUpdate, defineStore } from 'pinia'
import { Ref } from 'vue'
import { getPrices } from '~/common/api/coingeckoApi'

type PriceData = {
  hive: {
    usd: number | null
  }
  hive_dollar: {
    usd: number | null
  }
}

export const useTokenPricesStore = defineStore('tokenPrice', () => {
  const prices: Ref<PriceData> = ref({ hive: { usd: null }, hive_dollar: { usd: null } })

  const hivePrice = computed(() => prices.value?.hive?.usd)
  const hbdPrice = computed(() => prices.value?.hive_dollar?.usd)

  let isLoading = false
  const refreshTokenPrices = async () => {
    if (!isLoading && (!hivePrice.value || !hbdPrice.value)) {
      try {
        isLoading = true
        prices.value = await getPrices()
      } catch (error) {
        return error
      } finally {
        isLoading = false
      }
    }

    return prices.value
  }

  return {
    refreshTokenPrices,
    hivePrice,
    hbdPrice
  }
})

if (import.meta.hot) import.meta.hot.accept(acceptHMRUpdate(useTokenPricesStore, import.meta.hot))
