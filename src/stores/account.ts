import { defineStore } from 'pinia'
import { ref } from 'vue'
import { getAccounts, getOpenOrders, requestSign, updateSelectedWallet } from '~/common/api/hiveApi'

type AccountData = {
  name: string | null
  authenticated: boolean
  defaultWallet: string
  balance: string
  hbd_balance: string
  open_orders: Array<object>
}

export const useAccountStore = defineStore('account', () => {
  const account = ref<AccountData>({
    name: null,
    authenticated: false,
    defaultWallet: '',
    balance: '0 HIVE',
    hbd_balance: '0 HBD',
    open_orders: []
  })

  const initializeAccount = () => {
    const loggedInAccount = localStorage.getItem('account')
    if (loggedInAccount) {
      account.value.name = loggedInAccount
      account.value.authenticated = true
      setDefaultWallet()
      updateOpenOrders()
      updateBalances()
    }
  }

  const updateLoginState = async (user: string, wallet?: string) => {
    account.value.name = user
    account.value.authenticated = true
    localStorage.setItem('account', user)
    await setDefaultWallet(wallet)
    await updateBalances()
    await updateOpenOrders()
    location.reload()
  }

  const authenticate = async (wallet: any, user: string) => {
    return new Promise<void>(async (resolve, reject) => {
      try {
        const message = `${user}${Date.now()}`
        const keyRole = 'posting'

        if (wallet === 'keychain') {
          await requestSign(wallet, user, message, keyRole, 'Authenticate', result => {
            if (result && result.success) {
              updateLoginState(user, wallet)
            } else {
              reject(new Error('Keychain authentication failed'))
            }
          })
        } else if (wallet === 'peakvault') {
          const title = {
            title: 'Authenticate',
            message: 'The website wants to verify that you have the required authority on this account.'
          }
          const response = await requestSign(wallet, user, message, keyRole, title)
          if (response && response.success) {
            await updateLoginState(user, wallet)
            resolve()
          } else {
            reject(new Error('PeakVault authentication failed'))
          }
        }
      } catch (error) {
        reject(error)
      }
    })
  }

  const switchUser = async (user: string) => {
    await updateLoginState(user)
  }

  const signOut = () => {
    const storedWallets = localStorage.getItem('accountsWallet')
    const usersWallet = storedWallets ? JSON.parse(storedWallets) : {}
    const loggedUser = localStorage.getItem('account')

    if (loggedUser) {
      delete usersWallet[loggedUser]

      if (Object.keys(usersWallet).length === 0) {
        localStorage.removeItem('accountsWallet')
      } else {
        localStorage.setItem('accountsWallet', JSON.stringify(usersWallet))
      }
    }

    localStorage.removeItem('account')
    account.value = {
      name: null,
      authenticated: false,
      defaultWallet: '',
      balance: '0 HIVE',
      hbd_balance: '0 HBD',
      open_orders: []
    }
    location.reload()
  }

  // Functions to update balances and orders
  const updateBalances = async () => {
    if (account.value.name) {
      const accountData = await getAccounts([account.value.name]).then((result: [Account]) => result[0])
      account.value.balance = `${accountData.balance}`
      account.value.hbd_balance = `${accountData.hbd_balance}`
    }
  }

  const updateOpenOrders = async () => {
    if (account.value.name) {
      account.value.open_orders = await getOpenOrders([account.value.name])
    }
  }

  const setDefaultWallet = async (wallet?: string) => {
    if (!account.value.name) return

    let wallets = {}
    try {
      const storedWallets = localStorage.getItem('accountsWallet')
      if (storedWallets) {
        wallets = JSON.parse(storedWallets)
      }
    } catch (error) {
      console.error('Error parsing stored wallets', error)
    }

    account.value.defaultWallet = wallet || wallets[account.value.name] || ''

    if (wallet) {
      wallets[account.value.name] = wallet
      localStorage.setItem('accountsWallet', JSON.stringify(wallets))
    }

    await updateSelectedWallet(account.value.defaultWallet)
  }

  initializeAccount()

  return {
    account,
    authenticate,
    switchUser,
    signOut,
    updateBalances,
    updateOpenOrders,
    setDefaultWallet
  }
})
