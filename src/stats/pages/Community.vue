<template>
  <CommunityHeader :name="community" :community="communityData"></CommunityHeader>

  <header class="pt-6 pb-6" id="head">
    <div class="max-w-7xl mx-auto flex">
      <h1 class="text-3xl font-bold grow">Community Metrics</h1>
      <span class="mx-1"></span>
    </div>
  </header>

  <div>
    <div class="flex justify-between">
      <dl class="grid grid-cols-4 gap-2 sm:gap-3 mb-7">
        <a
          v-for="group in groups.filter(g => g.metrics.length)"
          :key="group.name"
          @click="selectedGroup = group"
          class="overflow-hidden rounded-lg py-3 px-7 shadow cursor-pointer dark:border dark:border-slate-500"
          :class="selectedGroup.name === group.name ? 'bg-emerald-500 dark:bg-emerald-300' : 'bg-white dark:bg-transparent'"
        >
          <dt
            class="truncate text-sm font-medium"
            :class="selectedGroup.name === group.name ? 'text-white dark:text-slate-800' : 'text-gray-500 dark:text-gray-300'"
          >
            {{ group.title }}
          </dt>
          <dd
            class="text-xl font-semibold tracking-tight"
            :class="selectedGroup.name === group.name ? 'text-white dark:text-slate-800' : 'text-gray-700 dark:text-gray-200'"
          >
            <span :class="selectedGroup.name === group.name ? 'text-white dark:text-slate-800' : 'text-emerald-500 dark:text-emerald-300'">{{
              group.metrics.length
            }}</span>
            stat{{ group.metrics.length > 1 ? 's' : '' }}
          </dd>
        </a>
      </dl>

      <a href="https://gitlab.com/peakd/hive-open-stats" target="_blank" rel="noopener" class="flex items-center mb-7">
        <div class="mr-2 flex-shrink-0"><img :src="StatsLogo" class="h-12 w-12 rounded-md" /></div>
        <div class="text-black text-base dark:text-white">
          <span>Powered by</span>
          <span class="block font-bold">Hive Open Stats</span>
        </div>
      </a>
    </div>

    <div class="flex flex-col">
      <div v-if="isLoading" class="-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8 mb-4">
        <div class="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
          <div class="overflow-hidden shadow ring-1 ring-black dark:ring-slate-700 ring-opacity-5 md:rounded-lg bg-white dark:bg-slate-800 p-5">
            <div class="text-center py-20 text-2xl text-gray-600 dark:text-gray-300">Loading ...</div>
          </div>
        </div>
      </div>
      <div v-else class="-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8 mb-4">
        <div class="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
          <div class="overflow-hidden shadow ring-1 ring-black dark:ring-slate-700 ring-opacity-5 md:rounded-lg bg-white dark:bg-slate-800 p-5">
            <div class="flex items-center mb-6">
              <div class="grow flex items-center">
                <Menu as="div" class="relative text-left">
                  <div>
                    <MenuButton class="flex items-center w-full font-medium">
                      <h3 class="text-2xl">{{ selectedMetric.name }}</h3>
                      <ChevronDownIcon class="-mr-1 ml-1 h-5 w-5" aria-hidden="true" />
                    </MenuButton>
                  </div>

                  <Transition
                    enter-active-class="transition ease-out duration-100"
                    enter-from-class="transform opacity-0 scale-95"
                    enter-to-class="transform opacity-100 scale-100"
                    leave-active-class="transition ease-in duration-75"
                    leave-from-class="transform opacity-100 scale-100"
                    leave-to-class="transform opacity-0 scale-95"
                  >
                    <MenuItems
                      class="absolute left-0 z-10 mt-2 w-56 origin-top-right rounded-md bg-white dark:bg-slate-800 shadow-lg ring-1 ring-black dark:ring-slate-700 ring-opacity-5 focus:outline-none overflow-y-auto"
                      style="max-height: calc(35vh + 1.25rem)"
                    >
                      <div class="py-1">
                        <MenuItem v-for="metric in selectedGroup.metrics" :key="metric.endpoint" v-slot="{ active }">
                          <a
                            @click="selectedMetric = metric"
                            :class="[
                              active ? 'bg-gray-100 dark:bg-slate-700 text-gray-900 dark:text-gray-100' : 'text-gray-700 dark:text-gray-200',
                              'block px-4 py-2 text-sm'
                            ]"
                            >{{ metric.name }}</a
                          >
                        </MenuItem>
                      </div>
                    </MenuItems>
                  </Transition>
                </Menu>

                <div v-if="selectedMetricDetails && selectedMetricDetails.length" class="relative flex flex-col items-center group ml-3">
                  <span class="inline-flex items-center rounded-full bg-slate-100 py-0.5 pl-2.5 pr-1 text-sm font-medium text-slate-700">
                    Details <InformationCircleIcon class="inline h-5 w-5 ml-1" />
                  </span>

                  <div class="absolute w-96 max-w-lg top-0 flex-col items-center hidden mt-6 group-hover:flex">
                    <div class="w-3 h-3 -mb-2 rotate-45 bg-slate-800 dark:bg-slate-700"></div>
                    <span
                      class="relative z-10 p-2 text-sm leading-none text-white dark:text-gray-800 whitespace-no-wrap bg-slate-800 dark:bg-gray-50 shadow rounded-md"
                    >
                      <div v-for="(details, index) in selectedMetricDetails" :key="index">
                        <hr v-if="index > 0" class="my-3 h-px bg-slate-600 border-0 dark:bg-slate-300" />
                        {{ details.desc }}
                      </div>
                    </span>
                  </div>
                </div>
              </div>

              <div class="isolate inline-flex items-center rounded-md shadow-sm">
                <button
                  @click="selectedTimeframe = 'daily'"
                  class="relative inline-flex items-center rounded-l-md border border-gray-300 dark:border-gray-500 bg-white dark:bg-slate-800 px-4 py-2 text-sm font-medium text-gray-400 hover:bg-gray-50 dark:hover:bg-slate-700 cursor-pointer"
                  :class="selectedTimeframe === 'daily' ? 'text-emerald-600 dark:text-emerald-300' : 'text-gray-500 dark:text-gray-300'"
                >
                  Daily
                  <span
                    aria-hidden="true"
                    :class="[selectedTimeframe === 'daily' ? 'bg-emerald-500' : 'bg-transparent', 'absolute inset-x-0 bottom-0 h-0.5']"
                  />
                </button>
                <button
                  @click="selectedTimeframe = 'weekly'"
                  class="relative -ml-px inline-flex items-center border border-gray-300 dark:border-gray-500 bg-white dark:bg-slate-800 px-4 py-2 text-sm font-medium hover:bg-gray-50 dark:hover:bg-slate-700 cursor-pointer"
                  :class="selectedTimeframe === 'weekly' ? 'text-emerald-600 dark:text-emerald-300' : 'text-gray-500 dark:text-gray-300'"
                >
                  Weekly
                  <span
                    aria-hidden="true"
                    :class="[selectedTimeframe === 'weekly' ? 'bg-emerald-500' : 'bg-transparent', 'absolute inset-x-0 bottom-0 h-0.5']"
                  />
                </button>
                <button
                  @click="selectedTimeframe = 'monthly'"
                  class="relative -ml-px inline-flex items-center border border-gray-300 dark:border-gray-500 bg-white dark:bg-slate-800 px-4 py-2 text-sm font-medium hover:bg-gray-50 dark:hover:bg-slate-700 cursor-pointer"
                  :class="selectedTimeframe === 'monthly' ? 'text-emerald-600 dark:text-emerald-300' : 'text-gray-500 dark:text-gray-300'"
                >
                  Monthly
                  <span
                    aria-hidden="true"
                    :class="[selectedTimeframe === 'monthly' ? 'bg-emerald-500' : 'bg-transparent', 'absolute inset-x-0 bottom-0 h-0.5']"
                  />
                </button>
                <button
                  @click="selectedTimeframe = 'yearly'"
                  class="relative -ml-px inline-flex items-center rounded-r-md border border-gray-300 dark:border-gray-500 bg-white dark:bg-slate-800 px-4 py-2 text-sm font-medium text-gray-400 hover:bg-gray-50 dark:hover:bg-slate-700 cursor-pointer"
                  :class="selectedTimeframe === 'yearly' ? 'text-emerald-600 dark:text-emerald-300' : 'text-gray-500 dark:text-gray-300'"
                >
                  Yearly
                  <span
                    aria-hidden="true"
                    :class="[selectedTimeframe === 'yearly' ? 'bg-emerald-500' : 'bg-transparent', 'absolute inset-x-0 bottom-0 h-0.5']"
                  />
                </button>
              </div>
            </div>

            <BarChart
              v-if="selectedMetric.chart === 'bar'"
              :key="`${selectedMetric.name}/${selectedTimeframe}`"
              :chartId="`${selectedMetric.name}/${selectedTimeframe}`"
              :labels="selectedMetric.labels(records)"
              :datasets="selectedMetricDatasets"
              :timeframe="selectedTimeframe"
              :yMinValues="
                selectedMetricDatasets.length > 1
                  ? { left: Math.min(...selectedMetricDatasets[0].data), right: Math.min(...selectedMetricDatasets[1].data) }
                  : null
              "
            />
            <div v-else class="text-lg text-medium text-gray-700 dark:text-gray-200 text-center">Chart not available for this metric</div>
          </div>
        </div>
      </div>

      <div v-if="records && records.length > 2" class="-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
          <div class="overflow-hidden shadow ring-1 ring-black dark:ring-slate-700 ring-opacity-5 md:rounded-lg">
            <table class="min-w-full divide-y divide-gray-300 dark:divide-gray-500">
              <thead class="bg-gray-50 dark:bg-slate-700">
                <tr>
                  <th
                    v-for="(key, index) in Object.keys(records[0])"
                    :key="index"
                    scope="col"
                    class="py-3 px-6 text-xs font-semibold text-gray-900 dark:text-gray-100"
                    :class="index === 0 ? 'text-left' : 'text-right'"
                  >
                    <span v-if="key === 'unit'">Period</span>
                    <span v-else>{{ splitAndCapitalize(key) }}</span>
                  </th>
                </tr>
              </thead>
              <tbody class="divide-y divide-gray-200 dark:divide-gray-700 bg-white dark:bg-slate-800">
                <tr v-for="(record, index) in records" :key="index">
                  <td
                    v-for="(value, index) in Object.values(record)"
                    :key="index"
                    class="whitespace-nowrap py-2 px-6 text-xs text-gray-900 dark:text-gray-100"
                    :class="index === 0 ? 'text-left' : 'text-right'"
                  >
                    {{ selectedMetric.formatter && selectedMetric.formatter[index] ? selectedMetric.formatter[index](value) : value }}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</template>

<script setup lang="ts">
  import { ChevronDownIcon, InformationCircleIcon } from '@heroicons/vue/solid'
  import { getCommunity, fetchData } from '~/stats/api/hiveOpenStatsApi'
  import orderBy from 'lodash.orderby'
  import groupBy from 'lodash.groupby'
  import StatsLogo from '~/assets/images/hive-open-stats.svg'
  import { formatNumber, splitAndCapitalize } from '~/common/helpers/formatter'
  import { hashcode } from '~/common/helpers/utils'

  const props = defineProps<{
    community: string
  }>()

  const community = props.community.startsWith('@') ? props.community.substring(1) : props.community

  const communityData = await getCommunity(community)

  console.log('Loading account transactions: ', community)

  const colors = ['#fcd34d', '#22577a', '#f94144', '#277da1', '#7209b7', '#4d908e', '#f8961e', '#90be6d', '#98c1d9', '#f3722c']

  const prepare = (records: Array<any>, field: string, parser: Function = (f: any) => f, sliceAt: number = 30) =>
    records
      .slice(0, sliceAt)
      .map((r: any) => (parser ? parser(r[field]) : r[field]))
      .reverse()

  const prepareMultiple = (
    name: string,
    records: Array<any>,
    fields: Array<string>,
    labels: Array<string> = ['Amount', 'Count'],
    charts: Array<string> = ['line', 'bar'],
    sliceAt: number = 30
  ) => {
    let startColor = hashcode(name)

    return fields.map((field, index) => ({
      type: charts[index],
      label: labels[index],
      data: prepare(records, field, parseFloat, sliceAt),
      borderColor: colors[(startColor + index) % colors.length],
      backgroundColor: colors[(startColor + index) % colors.length],
      lineTension: 0.25,
      ...(charts.length >= 2 && charts[0] !== charts[1] ? { yAxisID: index === 0 ? 'yLeft' : 'yRight' } : {})
    }))
  }

  const prepareSeries = (name: string, records: Array<any>, labelField: string, valueField: string, parser: Function = (f: any) => f, sliceAt: number = 30) => {
    const startColor = hashcode(name)
    const datasets = groupBy(records, (r: any) => r[labelField])
    return Object.keys(datasets).map((key, index) => ({
      label: key,
      borderColor: colors[(startColor + index) % colors.length],
      backgroundColor: colors[(startColor + index) % colors.length],
      data: datasets[key]
        .slice(0, sliceAt)
        .map((r: any) => parser(r[valueField]))
        .reverse()
    }))
  }

  //declaring metrics for the bar chart

  const subsMetrics: any[] = [
    {
      endpoint: community + '/subscribers_change',
      group: 'subscribers',
      details: ['community_subscribers_change'],
      name: 'Subscribers Change',
      sort: ['unit', 'desc'],
      formatter: [null, formatNumber, formatNumber],
      chart: 'bar',
      mapper: (results: Array<any>) => results.map(r => ({ unit: r.unit, ['Subscribers Change Number']: r.value, ['Cumulative Subscribers']: r.cumulative })),
      labels: (records: any) => prepare(records, 'unit'),
      datasets: (records: any) =>
        prepareMultiple(
          'Subscribers Change',
          records,
          ['Cumulative Subscribers', 'Subscribers Change Number'],
          ['Cumulative Subscribers', 'Subscribers Change Number'],
          ['line', 'bar']
        )
    }
  ]

  const commentsMetrics: any[] = [
    {
      endpoint: community + '/comments',
      group: 'comments',
      details: ['community_comments'],
      name: 'Community Comments',
      sort: ['unit', 'desc'],
      formatter: [null, formatNumber],
      chart: 'bar',
      mapper: (results: Array<any>) => results.map(r => ({ unit: r.unit, comment_number: r.value, cumulative_comments: r.cumulative })),
      labels: (records: any) => prepare(records, 'unit'),
      datasets: (records: any) =>
        prepareMultiple('Comments', records, ['cumulative_comments', 'comment_number'], ['Cumulative Created Comments', 'Created Comments'], ['line', 'bar'])
    },
    {
      endpoint: community + '/unique_comment_authors',
      group: 'comments',
      details: ['community_unique_comment_authors'],
      name: 'Unique Comments Authors',
      sort: ['unit', 'desc'],
      formatter: [null, formatNumber],
      chart: 'bar',
      mapper: (results: Array<any>) =>
        results.map(r => ({ unit: r.unit, daily_comment_authors: r.value, average_of_daily_comment_authors: r.average_of_daily_values })),
      labels: (records: any) => prepare(records, 'unit'),
      datasets: (records: any) =>
        prepareMultiple(
          'Unique Comments Authors',
          records,
          ['daily_comment_authors', 'average_of_daily_comment_authors'],
          ['Daily Comment Authors', 'Average Of Daily Comment Authors'],
          ['bar', 'bar']
        )
    }
  ]

  const postsMetrics: any[] = [
    {
      endpoint: community + '/posts',
      group: 'posts',
      details: ['community_posts'],
      name: 'Community Posts',
      sort: ['unit', 'desc'],
      formatter: [null, formatNumber],
      chart: 'bar',
      mapper: (results: Array<any>) => results.map(r => ({ unit: r.unit, post_number: r.value, cumulative_posts: r.cumulative })),
      labels: (records: any) => prepare(records, 'unit'),
      datasets: (records: any) =>
        prepareMultiple('Posts', records, ['cumulative_posts', 'post_number'], ['Cumulative Created Posts', 'Created Posts'], ['line', 'bar'])
    },
    {
      endpoint: community + '/unique_post_authors',
      group: 'posts',
      details: ['community_unique_post_authors'],
      name: 'Unique Post Authors',
      sort: ['unit', 'desc'],
      formatter: [null, formatNumber],
      chart: 'bar',
      mapper: (results: Array<any>) =>
        results.map(r => ({ unit: r.unit, daily_post_authors: r.value, average_of_daily_post_authors: r.average_of_daily_values })),
      labels: (records: any) => prepare(records, 'unit'),
      datasets: (records: any) =>
        prepareMultiple(
          'Unique Post Authors',
          records,
          ['daily_post_authors', 'average_of_daily_post_authors'],
          ['Daily Post Authors', 'Average Of Daily Post Authors'],
          ['bar', 'bar']
        )
    }
  ]

  const payoutsMetrics: any[] = [
    {
      endpoint: community + '/payouts',
      group: 'payouts',
      details: ['comment_rewards'],
      name: 'Post Payouts',
      sort: ['unit', 'desc'],
      formatter: [null, formatNumber, formatNumber],
      chart: 'bar',
      mapper: (results: Array<any>) =>
        results.map(r => ({ unit: r.unit, ['Post Payouts (HBD)']: r.post_payouts_hbd, ['Cumulative Post Payouts (HBD)']: r.post_payouts_hbd_cumulative })),
      labels: (records: any) => prepare(records, 'unit'),
      datasets: (records: any) =>
        prepareMultiple(
          'Post Payouts',
          records,
          ['Cumulative Post Payouts (HBD)', 'Post Payouts (HBD)'],
          ['Cumulative Post Payouts (HBD)', 'Post Payouts (HBD)'],
          ['line', 'bar']
        )
    },
    {
      endpoint: community + '/payouts',
      group: 'payouts',
      details: ['comment_rewards'],
      name: 'Comment Payouts',
      sort: ['unit', 'desc'],
      formatter: [null, formatNumber, formatNumber],
      chart: 'bar',
      mapper: (results: Array<any>) =>
        results.map(r => ({
          unit: r.unit,
          ['Comment Payouts (HBD)']: r.comment_payouts_hbd,
          ['Cumulative Comment Payouts (HBD)']: r.comment_payouts_hbd_cumulative
        })),
      labels: (records: any) => prepare(records, 'unit'),
      datasets: (records: any) =>
        prepareMultiple(
          'Comment Payouts',
          records,
          ['Cumulative Comment Payouts (HBD)', 'Comment Payouts (HBD)'],
          ['Cumulative Comment Payouts (HBD)', 'Comment Payouts (HBD)'],
          ['line', 'bar']
        )
    },
    {
      endpoint: community + '/payouts',
      group: 'payouts',
      details: ['community_payouts'],
      name: 'Total Payouts',
      sort: ['unit', 'desc'],
      formatter: [null, formatNumber, formatNumber],
      chart: 'bar',
      mapper: (results: Array<any>) =>
        results.map(r => ({ unit: r.unit, ['Total Payouts (HBD)']: r.total_payouts_hbd, ['Cumulative Total Payouts (HBD)']: r.total_payouts_hbd_cumulative })),
      labels: (records: any) => prepare(records, 'unit'),
      datasets: (records: any) =>
        prepareMultiple(
          'Total Payouts',
          records,
          ['Cumulative Total Payouts (HBD)', 'Total Payouts (HBD)'],
          ['Cumulative Total Payouts (HBD)', 'Total Payouts (HBD)'],
          ['line', 'bar']
        )
    }
  ]

  const metrics: any = [...subsMetrics, ...postsMetrics, ...commentsMetrics, ...payoutsMetrics]

  const groups = [
    { name: 'subscribers', title: 'Subscribers', metrics: metrics.filter((m: any) => m.group === 'subscribers') },
    { name: 'posts', title: 'Posts', metrics: metrics.filter((m: any) => m.group === 'posts') },
    { name: 'comments', title: 'Comments', metrics: metrics.filter((m: any) => m.group === 'comments') },
    { name: 'payouts', title: 'Payouts', metrics: metrics.filter((m: any) => m.group === 'payouts') }
  ]

  const route = useRoute()
  const router = useRouter()

  const records = ref([])

  const metricDetails = ref({})

  const selectedMetric = ref(route.query.metric ? metrics.find((m: any) => m.endpoint === route.query.metric) || metrics[0] : metrics[0])
  const selectedMetricDetails = computed(() => {
    if (!metricDetails.value || !selectedMetric.value) return []

    const metrics = selectedMetric.value.details || [selectedMetric.value.endpoint]
    return metrics.filter((m: String) => metricDetails.value[m]).map((m: String) => metricDetails.value[m])
  })
  const selectedMetricDatasets = computed(() => selectedMetric.value?.datasets(records.value))

  const selectedGroup = ref(groups.find(g => g.name === selectedMetric.value.group) || groups[0])
  const selectedTimeframe = ref(
    route.query.timeframe && ['daily', 'weekly', 'monthly', 'yearly'].includes(route.query.timeframe.toString()) ? route.query.timeframe.toString() : 'daily'
  )

  const isLoading = ref(false)

  const loadStats = async () => {
    isLoading.value = true

    records.value = []

    // use a custom fetch if provided, otherwise standard endpoint
    let results = selectedMetric.value.fetch
      ? await selectedMetric.value.fetch(selectedTimeframe.value)
      : await fetchData(`${selectedMetric.value.endpoint}/${selectedTimeframe.value}`)

    // custom mapper if needed
    if (selectedMetric.value.mapper) {
      results = selectedMetric.value.mapper(results)
    }

    // sort if needed
    if (selectedMetric.value.sort) {
      results = orderBy(results, selectedMetric.value.sort[0], selectedMetric.value.sort[1] || 'asc')
    }

    records.value = results

    isLoading.value = false
  }

  watch(selectedGroup, () => {
    selectedMetric.value = selectedGroup.value.metrics[0]
  })

  watch(selectedMetric, () => {
    router.push({ path: `/c/` + community, query: { metric: selectedMetric.value.endpoint, timeframe: selectedTimeframe.value } })
    loadStats()
  })

  watch(selectedTimeframe, () => {
    router.push({ path: `/c/` + community, query: { metric: selectedMetric.value.endpoint, timeframe: selectedTimeframe.value } })
    loadStats()
  })

  onMounted(() => {
    // load stats
    loadStats()
    // fetch details about provided metrics
    fetchData('metrics', 1000).then(result => (metricDetails.value = result))
  })
</script>
