/*
THIS DOCUMENT CONTAINS THE DEFINITION FOR AN INTERFACE FOR COMMUNITIES
DEFINITION THAT WILL BE USED IN THE "STATS" PROJECT
*/

export interface Community{
    name: string;
    type: number;
    title: string;
    description: string;
    about: string;
    language: string;
    nsfw: boolean;
    total_subscribers: number;
    total_posts: number;
    total_comments: number;
    average_daily_unique_post_authors: number;
    average_daily_unique_comment_authors: number;
    post_payouts_hbd: number;
    comment_payouts_hbd: number;
    total_payouts_hbd: number;



}