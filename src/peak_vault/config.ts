import { RouteRecordRaw } from 'vue-router'

const name = 'peakvault'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/peakvault-testing',
    name: 'peakvault-testing',
    component: () => import('~/peak_vault/WalletTest.vue'),
    meta: { title: 'Peakvault Testing' }
  }
]

export const config = {
  name: name,
  enabled: import.meta.env.VITE_APP_PEAKVAULT_TEST_ENABLED === 'true' ? true : false, // disabled by default
  routes: routes
}
