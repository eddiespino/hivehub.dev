import { createApp } from 'vue'
import App from '~/App.vue'
import VueTippy from 'vue-tippy'

import '~/assets/styles/index.css'

const app = createApp(App)

app.use(VueTippy, {
  directive: 'tippy', // => v-tippy
  component: 'tippy', // => <tippy/>
  componentSingleton: 'tippy-singleton' // => <tippy-singleton/>,
})

// init/config libraries
Object.values(import.meta.glob('~/config/*.ts', { eager: true }))
  .sort((m, n) => (n.priority || 0) - (m.priority || 0))
  .map(i => i.install?.({ app }))

app.mount('#app')
