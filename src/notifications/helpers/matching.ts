/**
 * Returns true if matchee has the same key-value pairs as matcher.
 * Matchee can also have different key-value paris not present in matcher.
 * @param matchee object to check
 * @param matcher properties to match
 * @returns
 */
export function matchesProperties(matchee: any, matcher: any): boolean {
  if (matchee === matcher) {
    return true
  }
  if (typeof matchee == 'object' && typeof matcher == 'object') {
    return Object.keys(matcher).every(key => {
      return matchesProperties(matchee[key], matcher[key])
    })
  }
  return false
}

/**
 * Returns true if object a is equal in every key-value pair to object b.
 * @param a object a
 * @param b object b
 * @returns
 */
export function matchesExactly(a: any, b: any): boolean {
  if (a === b) {
    return true
  }
  if (typeof a == 'object' && typeof b == 'object') {
    const matKeys = Object.keys(a)
    const propKeys = Object.keys(b)
    return (matKeys.length > propKeys.length ? matKeys : propKeys).every(key => {
      return matchesExactly(a[key], b[key])
    })
  }
  return false
}

/**
 * Returns whether AT LEAST ONE element of matchee matches AT LEAST ONE element of matcher
 * following the logic described in the matchingFunction. If the matching function is
 * matchesWithConditions then matcher must be in the form account.config
 * @param matchee matchee, to be matched
 * @param matcher reference
 * @param matchingFunction function handling the matching
 * @returns
 */
export function hasMatch(matchee: any | Array<any>, matcher: any | Array<any>, matchingFunction: Function): boolean {
  let limit_matchee = 1
  let limit_matcher = 1

  if (Array.isArray(matchee) && isOperation(matchee) == false) {
    limit_matchee = matchee.length
  } else {
    matchee = [matchee]
  }

  if (Array.isArray(matcher) && isOperation(matcher) == false) {
    limit_matcher = matcher.length
  } else {
    matcher = [matcher]
  }

  for (let i = 0; i < limit_matchee; i++) {
    for (let j = 0; j < limit_matcher; j++) {
      let isMatch = false
      if (matchingFunction.name == 'matchesWithConditions') {
        let conditions = {}
        // If no metadata use conditions, else use meta_conditions
        if (matchee[i][2] == undefined) {
          conditions = matcher[j].conditions ? matcher[j].conditions : {}
          isMatch = matchingFunction(matchee[i], [matcher[j].operation, {}], [{}, conditions])
        } else {
          conditions = matcher[j].meta_conditions ? matcher[j].meta_conditions : {}
          isMatch = matchingFunction(matchee[i][2], {}, conditions)
        }
        //isMatch = matchingFunction(matchee[i], matcher[j].operation, conditions)
      } else {
        isMatch = matchingFunction(matchee[i], matcher[j])
      }
      if (isMatch) {
        return true
      }
    }
  }

  return false
}

/**
 * Returns all the elemets of matcher that match AT LEAST ONE element of matchee
 * following the logic described in the matchingFunction. If the matching function is
 * matchesWithConditions then matcher must be in the form account.config
 * @param matchee matchee, to be matched
 * @param matcher reference
 * @param matchingFunction function handling the matching
 * @param stopAtFirst if true only the first match will be returned
 * @returns
 */
export function findByMatching(matchee: any | Array<any>, matcher: any | Array<any>, matchingFunction: Function, stopAtFirst: boolean = false): Array<any> {
  let limit_matchee = 1
  let limit_matcher = 1
  let result = []

  if (Array.isArray(matchee) && isOperation(matchee) == false) {
    limit_matchee = matchee.length
  } else {
    matchee = [matchee]
  }

  if (Array.isArray(matcher) && isOperation(matcher) == false) {
    limit_matcher = matcher.length
  } else {
    matcher = [matcher]
  }

  for (let i = 0; i < limit_matchee; i++) {
    for (let j = 0; j < limit_matcher; j++) {
      let isMatch = false
      if (matchingFunction.name == 'matchesWithConditions') {
        const conditions = matcher[j].conditions ? matcher[j].conditions : {}
        //isMatch = matchingFunction(matchee[i], matcher[j].operation, conditions)
        isMatch = matchingFunction(matchee[i], [matcher[j].operation, {}], [{}, conditions])
      } else {
        isMatch = matchingFunction(matchee[i], matcher[j])
      }
      if (isMatch) {
        result.push(matcher[j])
        if (stopAtFirst) {
          return result
        }
      }
    }
  }

  return result
}

/**
 * Returns true if the object has the shape of a blockchain operation
 * @param obj
 * @returns
 */
export function isOperation(obj: any): boolean {
  return Array.isArray(obj) && (obj.length == 2 || obj.length == 3) && typeof obj[0] == 'string' && typeof obj[1] == 'object'
}
