import axios from 'axios'
const url = 'https://chat-api.peakd.com/api'

export const getChatNotifications = async (user: string): Promise<any> => {
  const response = await axios.get(`${url}/readNotifications/${user}`)
  return response.data[1]
}

export const getLastNotificationTimestamp = async (user: string): Promise<any> => {
  const response = await axios.get(`${url}/readNotificationCount/${user}`)
  return response.data
}
