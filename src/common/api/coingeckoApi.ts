import axios from 'axios'

const baseUrl = 'https://api.coingecko.com/api/v3'

export const getPrices = async (tokens = ['hive', 'hive_dollar'], includeChange = false) => {
  const response = await axios.get(`${baseUrl}/simple/price`, {
    params: { ids: tokens.join(','), vs_currencies: 'usd', include_24hr_change: includeChange }
  })
  return response.data
}
