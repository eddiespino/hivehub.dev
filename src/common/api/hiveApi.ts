import { capitalize } from './../helpers/formatter'
import axios from 'axios'
import bufferFrom from 'buffer-from'
import {
  Client,
  BlockHeader,
  SignedBlock,
  SignedTransaction,
  AppliedOperation,
  DynamicGlobalProperties,
  KeyRole,
  utils,
  Operation,
  Transaction,
  TransactionConfirmation
} from '@hiveio/dhive'
import { getAvailableWallets, getWallet } from '@peakd/hive-wallet-sdk'
import { AbortError } from 'p-retry'
import { Wallet } from '@peakd/hive-wallet-sdk/dist/modules/Wallet'

const isTestnet = window.location.hostname.startsWith('testnet') || window.location.hostname.startsWith('mirrornet')
const apiNodes = ['https://api.deathwing.me', 'https://hived.emre.sh', 'https://api.hive.blog', 'https://api.openhive.network']

const nodes = isTestnet ? ['https://api.fake.openhive.network'] : apiNodes
let client = new Client(nodes, { failoverThreshold: nodes.length, timeout: 1 })

const defaultWallet = 'keychain'
let wallet: Wallet | string = defaultWallet

/*
export const fetch = async (api: string, method: string, params: any[]): Promise<any> => {
  const run = async () => {
    try {
      const result = await pTimeout(client.call(api, method, ...params), 1500)
      console.debug('API result: ', result)
      return result
    } catch (error) {
      console.error('API node error: ', error)
      throw error
    }
  }

  return pRetry(run, { retries: 3 })
}
*/

export interface FullTransaction extends SignedTransaction {
  block_num: number
  transaction_id: string
  virtual?: boolean
}

export const getDynamicGlobalProperties = async (): Promise<DynamicGlobalProperties> => {
  return await client.database.getDynamicGlobalProperties()
}

export const getHardforkVersion = async (): Promise<any> => {
  return await client.call('condenser_api', 'get_hardfork_version', [])
}
export const getTransactionStatus = async (transaction_id: string): Promise<any> => {
  return await client.call('transaction_status_api', 'find_transaction', { transaction_id })
}

export const getBlockHeader = async (blockNum: number): Promise<BlockHeader> => {
  return await client.database.getBlockHeader(blockNum)
}

export const getBlock = async (blockNum: number): Promise<SignedBlock> => {
  return await client.database.getBlock(blockNum)
}

export const getOpsInBlock = async (blockNum: number, onlyVirtual: boolean = false) => {
  return await client.call('condenser_api', 'get_ops_in_block', [blockNum, onlyVirtual])
}

export const getOrderBook = async (limit: number = 500) => {
  return await client.call('condenser_api', 'get_order_book', [limit])
}

export const getBlocks = async (start: number, count: number): Promise<any> => {
  return await client.call('block_api', 'get_block_range', { starting_block_num: start, count: count })
}

export const getTransaction = async (trxId: string): Promise<FullTransaction> => {
  return (await client.database.getTransaction(trxId)) as FullTransaction
}

export const getAccountHistory = async (account: string, start: number = -1, limit: number = 250): Promise<[[number, AppliedOperation]]> => {
  /*
  const op = utils.operationOrders
  const operationsBitmask: [number, number] = utils.makeBitMaskFilter([op.custom_json])

  return await client.database.getAccountHistory(account, start, limit, operationsBitmask)
  */
  return await client.database.getAccountHistory(account, start, limit)
}

export const getWitnesses = async (last: string = '', limit: number = 100): Promise<any> => {
  return await client.call('condenser_api', 'get_witnesses_by_vote', [last, limit])
}

export const getActiveWitnesses = async (): Promise<any> => {
  return await client.call('condenser_api', 'get_active_witnesses', [])
}

export const getVolume = async (): Promise<any> => {
  return await client.call('condenser_api', 'get_volume', [])
}

export const getProposals = async (
  start: Array<Number> = [-1],
  limit: number = 1000,
  order: string = 'by_total_votes',
  order_direction: string = 'descending',
  status: string = 'votable'
): Promise<any> => {
  return await client.call('condenser_api', 'list_proposals', [start, limit, order, order_direction, status])
}

export const getAccounts = async (account: Array<String>): Promise<any> => {
  return await client.call('condenser_api', 'get_accounts', [account])
}

export const getOpenOrders = async (account: Array<String>): Promise<any> => {
  return await client.call('condenser_api', 'get_open_orders', account)
}

export const getResourceCredits = async (account: string): Promise<any> => {
  return await client.call('rc_api', 'find_rc_accounts', { accounts: [account] })
}

export const getPost = async (author: string, permlink: string): Promise<any> => {
  return await client.call('bridge', 'get_post', { author: author, permlink: permlink })
}

export const getTicker = async () => {
  return await client.call('condenser_api', 'get_ticker', [])
}

export const getAccountSubscriptions = async (account: string) => {
  return await client.call('bridge', 'list_all_subscriptions', { account })
}

export const getTradeHistory = async (limit: number = 1000, startDate: string): Promise<any> => {
  const endDate = new Date().toISOString().slice(0, 19)
  let returnedData = await client.call('condenser_api', 'get_trade_history', [startDate, endDate, limit])
  if (!returnedData[0]) {
    const today = new Date(startDate)
    const date = new Date(today.setHours(today.getHours() - (today.getTimezoneOffset() / 60 + 1)))
    const seconds = `${date.getUTCSeconds()}`.length === 1 ? `0${date.getUTCSeconds()}` : `${date.getUTCSeconds()}`
    let yesterday =
      date.getUTCFullYear() + '-' + (date.getUTCMonth() + 1) + '-' + date.getUTCDate() + 'T' + date.getUTCHours() + ':' + date.getUTCMinutes() + ':' + seconds
    returnedData = await getTradeHistory(limit, yesterday)
  }
  if (returnedData.length < limit) {
    return returnedData
  } else {
    return returnedData.concat(await getTradeHistory(limit, returnedData.at(-1).date))
  }
}
// Can be used like this:
// const trades = await getAccountLastTrades('hiveio')
// console.log('Trades: ', trades)
export const getAccountLastTrades = async (account: string) => {
  const filter = utils.makeBitMaskFilter([utils.operationOrders.fill_order])
  try {
    return await client.database.getAccountHistory(account, -1, 1000, filter)
  } catch (error) {
    // we can get an error if no result found in the latest 2000 operations. We can iterate the API call or return an empty array
    return []
  }
}

export const getApiNodes = async (): Promise<any> => {
  const response = await axios.get('https://beacon.peakd.com/api/nodes')
  return response.data
}

export const getAccountsFromKeys = async (keys: string[]): Promise<any> => {
  return await client.call('account_by_key_api', 'get_key_references', { keys: keys })
}

export const getInstalledWallets = async () => {
  const wallets = await getAvailableWallets()
  return Object.keys(wallets)
}

export const checkForKeychain = () => {
  if (!window.hive_keychain) {
    return Promise.reject('Keychain not found')
  }
}

export const requestSign = async (wallet: any, username: string, message: any, key: KeyRole, title: any, callback?: Function) => {
  if (wallet !== defaultWallet) {
    wallet = await getWallet('peakvault')
    return await wallet.requestSignBuffer(username, capitalize(key), message, title)
  } else {
    return new Promise(() => {
      window.hive_keychain.requestSignBuffer(username, message, capitalize(key), callback)
    })
  }
}

export const reqPost = async (
  author: string,
  title: string,
  body: string,
  parentPerm: string,
  parentAuthor: string,
  jsonMetadata: any,
  permlink: string,
  callback?: Function
) => {
  if (wallet !== defaultWallet) {
    return await wallet.requestPost(author, title, body, parentPerm, parentAuthor, jsonMetadata, permlink)
  } else {
    return new Promise((resolve, reject) => {
      window.hive_keychain.requestPost(author, title, body, parentPerm, parentAuthor, JSON.stringify(jsonMetadata), permlink, '', (result: any) => {
        return result.success ? resolve(result) : reject(result)
      })
    })
  }
}

export const sendCustomJson = async (username: string, id: string, key: KeyRole | string, json: string, message: string) => {
  if (wallet !== defaultWallet) {
    return await wallet.requestCustomJson(username, id, key, JSON.stringify(json), message).catch((response: any) => {
      if (response.error && response.error === 'user_cancel') {
        throw new AbortError(response.error)
      }
    })
  } else {
    return new Promise((resolve, reject) => {
      window.hive_keychain.requestCustomJson(username, id, capitalize(key), json, message, (result: any) => (result.success ? resolve(result) : reject(result)))
    })
  }
}

export const signTrx = async (username: string, trx: any, role: KeyRole | string) => {
  if (wallet !== defaultWallet) {
    return 'Operations not supported'
  }
  return new Promise((resolve, reject) => {
    window.hive_keychain.requestSignTx(username, trx, role, (result: any) => {
      return result.success ? resolve(result) : reject(result)
    })
  })
}

export const broadcast = async (username: string, operations: any[], key: KeyRole, title: string, callback?: Function) => {
  if (wallet !== defaultWallet) {
    return await wallet.requestBroadcast(username, operations, capitalize(key), title).catch((response: any) => {
      if (response.error && response.error === 'user_cancel') {
        throw new AbortError(response.error)
      }
    })
  }
  return new Promise((resolve, reject) => {
    window.hive_keychain.requestBroadcast(username, operations, capitalize(key), callback)
  })
}

export const prepareTrx = async (operations: Operation[], extensions: any[] = [], expireTime: number = 1000 * 3590): Promise<Transaction> => {
  const props = await client.database.getDynamicGlobalProperties()

  return {
    ref_block_num: props.head_block_number & 0xffff,
    ref_block_prefix: bufferFrom(props.head_block_id, 'hex').readUInt32LE(4),
    expiration: new Date(Date.now() + expireTime).toISOString().slice(0, -5),
    extensions: extensions,
    operations
  }
}

export const sendTrx = async (signedTrx: SignedTransaction): Promise<TransactionConfirmation> => {
  return client.broadcast.send(signedTrx)
}

export const hasEnoughSignatures = async (trx: SignedTransaction): Promise<boolean> => {
  try {
    return await client.database.verifyAuthority(trx)
  } catch (err) {
    return false
  }
}

export const updateHiveEndpoint = (endpoint: string) => {
  if (isTestnet) return

  const newNodes = [endpoint].concat(nodes)
  client = new Client(newNodes, { failoverThreshold: newNodes.length, timeout: 1 })
}

export const updateSelectedWallet = async (selectedWallet?: string) => {
  if (selectedWallet && selectedWallet !== defaultWallet) {
    wallet = await getWallet('peakvault')
  } else {
    wallet = defaultWallet
  }
}
