// list from hive-js (https://gitlab.syncad.com/hive/hive-js/-/blob/master/src/broadcast/operations.js)
export const operations = [
  {
    roles: ['posting', 'active', 'owner'],
    operation: 'vote',
    params: ['voter', 'author', 'permlink', 'weight'],
    multisign: true,
    peakvault: true,
    prepare: (params: any) => ({
      ...params,
      weight: parseInt(params.weight)
    })
  },
  {
    roles: ['posting', 'active', 'owner'],
    operation: 'comment',
    params: ['parent_author', 'parent_permlink', 'author', 'permlink', 'title', 'body', 'json_metadata'],
    multisign: true,
    peakvault: true
  },
  {
    roles: ['active', 'owner'],
    operation: 'transfer',
    params: ['from', 'to', 'amount', 'currency', 'memo'],
    multisign: true,
    peakvault: true,
    prepare: (params: any) => ({
      ...params,
      amount: `${parseFloat(params.amount).toFixed(3)} ${params.currency}`
    })
  },
  {
    roles: ['active', 'owner'],
    operation: 'transfer_to_vesting',
    params: ['from', 'to', 'amount']
  },
  {
    roles: ['active', 'owner'],
    operation: 'withdraw_vesting',
    params: ['account', 'vesting_shares']
  },
  {
    roles: ['active', 'owner'],
    operation: 'limit_order_create',
    params: ['owner', 'orderid', 'amount_to_sell', 'min_to_receive', 'fill_or_kill', 'expiration']
  },
  {
    roles: ['active', 'owner'],
    operation: 'limit_order_cancel',
    params: ['owner', 'orderid']
  },
  {
    roles: ['active', 'owner'],
    operation: 'price',
    params: ['base', 'quote']
  },
  {
    roles: ['active', 'owner'],
    operation: 'feed_publish',
    params: ['publisher', 'exchange_rate']
  },
  {
    roles: ['active', 'owner'],
    operation: 'convert',
    params: ['owner', 'requestid', 'amount']
  },
  {
    roles: ['active', 'owner'],
    operation: 'collateralized_convert',
    params: ['owner', 'requestid', 'amount']
  },
  {
    roles: ['active', 'owner'],
    operation: 'account_create',
    params: ['fee', 'creator', 'new_account_name', 'owner', 'active', 'posting', 'memo_key', 'json_metadata']
  },
  {
    roles: ['active', 'owner'],
    operation: 'account_update',
    params: ['account', 'owner', 'active', 'posting', 'memo_key', 'json_metadata']
  },
  {
    roles: ['active', 'owner'],
    operation: 'witness_update',
    params: ['owner', 'url', 'block_signing_key', 'props', 'fee']
  },
  {
    roles: ['active', 'owner'],
    operation: 'account_witness_vote',
    params: ['account', 'witness', 'approve'],

    prepare: (params: any) => ({
      ...params,
      approve: params.approve.toLowerCase() === 'true' ? true : false
    })
  },
  {
    roles: ['active', 'owner'],
    operation: 'account_witness_proxy',
    params: ['account', 'proxy']
  },
  {
    roles: ['active', 'owner'],
    operation: 'pow',
    params: ['worker', 'input', 'signature', 'work']
  },
  {
    roles: ['active', 'owner'],
    operation: 'custom',
    params: ['required_auths', 'id', 'data']
  },
  {
    roles: ['posting', 'active', 'owner'],
    operation: 'delete_comment',
    params: ['author', 'permlink']
  },
  {
    roles: ['posting', 'active', 'owner'],
    operation: 'custom_json',
    params: ['required_auths', 'required_posting_auths', 'id', 'json'],
    multisign: true,
    peakvault: true,
    prepare: (params: any) => ({
      ...params,
      required_auths: `[${params.required_auths}]`,
      required_posting_auths: `[${params.required_posting_auths}]`
    })
  },
  {
    roles: ['posting', 'active', 'owner'],
    operation: 'comment_options',
    params: ['author', 'permlink', 'max_accepted_payout', 'percent_hbd', 'allow_votes', 'allow_curation_rewards', 'extensions']
  },
  {
    roles: ['active', 'owner'],
    operation: 'set_withdraw_vesting_route',
    params: ['from_account', 'to_account', 'percent', 'auto_vest']
  },
  {
    roles: ['active', 'owner'],
    operation: 'limit_order_create2',
    params: ['owner', 'orderid', 'amount_to_sell', 'exchange_rate', 'fill_or_kill', 'expiration']
  },
  {
    roles: ['active', 'owner'],
    operation: 'claim_account',
    params: ['creator', 'fee', 'extensions']
  },
  {
    roles: ['active', 'owner'],
    operation: 'create_claimed_account',
    params: ['creator', 'new_account_name', 'owner', 'active', 'posting', 'memo_key', 'json_metadata', 'extensions']
  },
  {
    roles: ['active', 'owner'],
    operation: 'request_account_recovery',
    params: ['recovery_account', 'account_to_recover', 'new_owner_authority', 'extensions']
  },
  {
    roles: ['owner'],
    operation: 'recover_account',
    params: ['account_to_recover', 'new_owner_authority', 'recent_owner_authority', 'extensions']
  },
  {
    roles: ['owner'],
    operation: 'change_recovery_account',
    params: ['account_to_recover', 'new_recovery_account', 'extensions']
  },
  {
    roles: ['active', 'owner'],
    operation: 'escrow_transfer',
    params: ['from', 'to', 'agent', 'escrow_id', 'hbd_amount', 'hive_amount', 'fee', 'ratification_deadline', 'escrow_expiration', 'json_meta']
  },
  {
    roles: ['active', 'owner'],
    operation: 'escrow_dispute',
    params: ['from', 'to', 'agent', 'who', 'escrow_id']
  },
  {
    roles: ['active', 'owner'],
    operation: 'escrow_release',
    params: ['from', 'to', 'agent', 'who', 'receiver', 'escrow_id', 'hbd_amount', 'hive_amount']
  },
  {
    roles: ['active', 'owner'],
    operation: 'pow2',
    params: ['input', 'pow_summary']
  },
  {
    roles: ['active', 'owner'],
    operation: 'escrow_approve',
    params: ['from', 'to', 'agent', 'who', 'escrow_id', 'approve']
  },
  {
    roles: ['active', 'owner'],
    operation: 'transfer_to_savings',
    params: ['from', 'to', 'amount', 'memo']
  },
  {
    roles: ['active', 'owner'],
    operation: 'transfer_from_savings',
    params: ['from', 'request_id', 'to', 'amount', 'memo']
  },
  {
    roles: ['active', 'owner'],
    operation: 'cancel_transfer_from_savings',
    params: ['from', 'request_id']
  },
  {
    roles: ['posting', 'active', 'owner'],
    operation: 'custom_binary',
    params: ['id', 'data']
  },
  {
    roles: ['owner'],
    operation: 'decline_voting_rights',
    params: ['account', 'decline']
  },
  {
    roles: ['active', 'owner'],
    operation: 'reset_account',
    params: ['reset_account', 'account_to_reset', 'new_owner_authority']
  },
  {
    roles: ['owner', 'posting'],
    operation: 'set_reset_account',
    params: ['account', 'current_reset_account', 'reset_account']
  },
  {
    roles: ['posting', 'active', 'owner'],
    operation: 'claim_reward_balance',
    params: ['account', 'reward_hive', 'reward_hbd', 'reward_vests']
  },
  {
    roles: ['active', 'owner'],
    operation: 'delegate_vesting_shares',
    params: ['delegator', 'delegatee', 'vesting_shares']
  },
  {
    roles: ['active', 'owner'],
    operation: 'account_create_with_delegation',
    params: ['fee', 'delegation', 'creator', 'new_account_name', 'owner', 'active', 'posting', 'memo_key', 'json_metadata', 'extensions']
  },
  {
    roles: ['active', 'owner'],
    operation: 'witness_set_properties',
    params: ['owner', 'props', 'extensions']
  },
  {
    roles: ['posting', 'active', 'owner'],
    operation: 'account_update2',
    params: ['account', 'owner', 'active', 'posting', 'memo_key', 'json_metadata', 'posting_json_metadata', 'extensions']
  },
  {
    roles: ['active', 'owner'],
    operation: 'create_proposal',
    params: ['creator', 'receiver', 'start_date', 'end_date', 'daily_pay', 'subject', 'permlink', 'extensions']
  },
  {
    roles: ['active', 'owner'],
    operation: 'update_proposal_votes',
    params: ['voter', 'proposal_ids', 'approve', 'extensions']
  },
  {
    roles: ['active', 'owner'],
    operation: 'remove_proposal',
    params: ['proposal_owner', 'proposal_ids', 'extensions']
  },
  {
    roles: ['active', 'owner'],
    operation: 'update_proposal',
    params: ['proposal_id', 'creator', 'daily_pay', 'subject', 'permlink', 'extensions']
  },
  {
    roles: ['active', 'owner'],
    operation: 'recurrent_transfer',
    params: ['from', 'to', 'amount', 'memo', 'recurrence', 'executions', 'extensions']
  },
  {
    roles: ['posting', 'active', 'owner'],
    operation: 'claim_reward_balance2',
    params: ['account', 'reward_tokens', 'extensions']
  },
  {
    roles: ['posting', 'active', 'owner'],
    operation: 'vote2',
    params: ['voter', 'author', 'permlink', 'rshares', 'extensions']
  },
  {
    roles: ['active', 'owner'],
    operation: 'smt_create',
    params: ['control_account', 'symbol', 'smt_creation_fee', 'precision', 'extensions']
  },
  {
    roles: ['active', 'owner'],
    operation: 'smt_setup',
    params: [
      'control_account',
      'symbol',
      'max_supply',
      'contribution_begin_time',
      'contribution_end_time',
      'launch_time',
      'hive_units_min',
      'min_unit_ratio',
      'max_unit_ratio',
      'extensions'
    ]
  },
  {
    roles: ['active', 'owner'],
    operation: 'smt_setup_emissions',
    params: [
      'control_account',
      'symbol',
      'schedule_time',
      'emissions_unit',
      'interval_seconds',
      'interval_coount',
      'lep_time',
      'rep_time',
      'lep_abs_amount',
      'rep_abs_amount',
      'lep_rel_amount_numerator',
      'rep_rel_amount_numerator',
      'rel_amount_denom_bits',
      'remove',
      'floor_emissions',
      'extensions'
    ]
  },
  {
    roles: ['active', 'owner'],
    operation: 'smt_setup_ico_tier',
    params: ['control_account', 'symbol', 'hive_units_cap', 'generation_policy', 'remove', 'extensions']
  },
  {
    roles: ['active', 'owner'],
    operation: 'smt_set_setup_parameters',
    params: ['control_account', 'symbol', 'setup_parameters', 'extensions']
  },
  {
    roles: ['active', 'owner'],
    operation: 'smt_set_runtime_parameters',
    params: ['control_account', 'symbol', 'runtime_parameters', 'extensions']
  },
  {
    roles: ['active', 'owner'],
    operation: 'smt_contribute',
    params: ['contributor', 'symbol', 'contribution_id', 'contribution', 'extensions']
  },
  {
    roles: ['active', 'owner'],
    operation: 'fill_convert_request',
    params: ['owner', 'requestid', 'amount_in', 'amount_out']
  },
  {
    roles: ['posting', 'active', 'owner'],
    operation: 'comment_reward',
    params: ['author', 'permlink', 'payout']
  },
  {
    roles: ['active', 'owner'],
    operation: 'liquidity_reward',
    params: ['owner', 'payout']
  },
  {
    roles: ['active', 'owner'],
    operation: 'interest',
    params: ['owner', 'interest']
  },
  {
    roles: ['active', 'owner'],
    operation: 'fill_vesting_withdraw',
    params: ['from_account', 'to_account', 'withdrawn', 'deposited']
  },
  {
    roles: ['posting', 'active', 'owner'],
    operation: 'fill_order',
    params: ['current_owner', 'current_orderid', 'current_pays', 'open_owner', 'open_orderid', 'open_pays']
  },
  {
    roles: ['posting', 'active', 'owner'],
    operation: 'fill_transfer_from_savings',
    params: ['from', 'to', 'amount', 'request_id', 'memo']
  }
]

export const mapOpsParams = (op: string, param: string) => {
  switch (op) {
    case 'vote':
      switch (param) {
        case 'voter':
          return 'Account casting the vote.'
        case 'author':
          return 'Account that created the content (post or comment).'
        case 'permlink':
          return 'Permalink or unique identifier of the content.'
        case 'weight':
          return 'Weight of the vote,  full upvote is often represented as 10000.'
        default:
          return
      }
    case 'comment':
      switch (param) {
        case 'parent_author':
          return 'Account of the author of the post or comment to which the new comment is being added. If the comment is a reply to a post, this field would be empty.'
        case 'parent_permlink':
          return 'Unique identifier (permlink) of the post or comment to which the new comment is being added.'
        case 'author':
          return 'Account of the user adding the comment.'
        case 'permlink':
          return 'Unique identifier for the comment.'
        case 'title':
          return 'Optional title for the comment.'
        case 'body':
          return 'Content of the comment.'
        case 'json_metadata':
          return 'Optional additional metadata in JSON format, which can include information such as tags, images, and other custom data.'
        default:
          return ''
      }
    case 'transfer':
      switch (param) {
        case 'from':
          return 'Account that initiates the transfer.'
        case 'to':
          return 'Account that receives the transferred funds.'
        case 'amount':
          return 'Amount of token being transferred. It includes only the numerical amount (e.g. 27.290).'
        case 'currency':
          return 'Shorthand notation of the token being transferred. It includes the currency symbol (e.g. PKM).'
        case 'memo':
          return 'Short message or note attached to the transfer'
        default:
          return ''
      }
    case 'account_witness_vote':
      switch (param) {
        case 'account':
          return 'Account of the witness for whom the vote is being cast.'
        case 'Witness':
          return 'Witness to be voted.'
        case 'approve':
          return 'True to approve or false to disapprove.'
        default:
          return ''
      }
    case 'account_witness_proxy':
      switch (param) {
        case 'account':
          return 'Account that wants to set or update its witness proxy.'
        case 'proxy':
          return 'Account that will act as the proxy for witness voting on behalf of the specified account.'
        default:
          return ''
      }
    case 'custom_json':
      switch (param) {
        case 'required_auths':
          return 'Array of accounts that must sign the operation.'
        case 'required_posting_auths':
          return 'Array of accounts that must have posting authority to perform the operation.'
        case 'id':
          return 'String identifier for the operation. This can be used to differentiate between different types of custom JSON operations.'
        case 'json':
          return 'Actual JSON-formatted data that will be stored on the blockchain. This is where the custom data or instructions for a DApp or custom application would be placed.'
        default:
          return ''
      }
    default:
      return ''
  }
}
