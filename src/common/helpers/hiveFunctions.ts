import { Asset } from '@hiveio/dhive'
import { Account } from '@hiveio/dhive/lib/chain/account'
import { fromUnixTime, compareAsc } from 'date-fns'
import { zonedTimeToUtc } from 'date-fns-tz'
import { useMarketStore } from '~/stores/market'

export const vestToHive = (totalVestingHive: string | number, vestingShares: string | number, totalVestingShare: string | number) => {
  return (parseFloat(`${totalVestingHive}`) * parseFloat(`${vestingShares}`)) / parseFloat(`${totalVestingShare}`)
}

export const parseAmount = (value: string | Asset) => {
  if (!value) return 0

  return value instanceof Asset ? value.amount : parseFloat(value)
}

// Calculate RC
export const calculateRcMana = (lastUpdateTime: number, max: string, current: string) => {
  // last_mana + (now - last_update) * max_mana / 5 days
  const elapsed = Date.now() / 1000 - lastUpdateTime
  const maxRc = parseFloat(max)
  let currentRc = parseFloat(current) + (elapsed * maxRc) / 432000
  if (currentRc > maxRc) {
    currentRc = maxRc
  }

  return {
    ratioRc: currentRc / maxRc,
    maxRc: maxRc,
    currentRc: currentRc
  }
}

// Calculate vote and downvote power
export const calculateEffectiveVestingShares = (account: any) => {
  let effectiveVestingShares = parseFloat(account.vesting_shares) + parseFloat(account.received_vesting_shares) - parseFloat(account.delegated_vesting_shares)

  // if there is a power down occurring, also reduce effective vesting shares by this week's power down amount
  const timezone = 'Europe/London'
  if (compareAsc(zonedTimeToUtc(account.next_vesting_withdrawal, timezone), zonedTimeToUtc(fromUnixTime(0), timezone)) === 1) {
    // reduce by minimum between 'weekly amount' and 'reminder'
    effectiveVestingShares -= Math.min(parseFloat(account.vesting_withdraw_rate), parseFloat(account.to_withdraw) - parseFloat(account.withdrawn))
  }

  return effectiveVestingShares
}
export const nonRepeatingAsks = computed(() => {
  const marketStore = useMarketStore()

  if (!marketStore?.market?.orderBook?.asks) return []

  const asks: any[] = []
  marketStore.market.orderBook.asks.forEach(obj => {
    let inArray = asks.find(ask => parseFloat(obj.real_price).toFixed(5) === parseFloat(ask.real_price).toFixed(5))
    if (inArray) {
      inArray.hbd += obj.hbd
      inArray.hive += obj.hive
      inArray.order_price.base =
        parseFloat(inArray.order_price.base.split(' ')[0]) + parseFloat(obj.order_price.base.split(' ')[0]) + obj.order_price.base.split(' ')[1]
      inArray.order_price.quote =
        parseFloat(inArray.order_price.quote.split(' ')[0]) + parseFloat(obj.order_price.quote.split(' ')[0]) + obj.order_price.quote.split(' ')[1]
      inArray.real_price = parseFloat(obj.real_price).toFixed(5)
    } else {
      asks.push(JSON.parse(JSON.stringify(obj)))
    }
  })

  return asks
})
export const nonRepeatingAsksReduced = computed(() =>
  nonRepeatingAsks.value.map((b, index, array) => {
    const volume = array.reduce((result, obj, i) => (i <= index ? result + obj.hbd / 1000 : result), 0)
    return [parseFloat(parseFloat(b.real_price).toFixed(5)), volume]
  })
)

export const nonRepeatingBids = computed(() => {
  const marketStore = useMarketStore()

  if (!marketStore?.market?.orderBook?.bids) return []

  const bids: any[] = []
  marketStore.market.orderBook.bids.forEach(obj => {
    let inArray = bids.find(bid => parseFloat(obj.real_price).toFixed(5) === parseFloat(bid.real_price).toFixed(5))
    if (inArray) {
      inArray.hbd += obj.hbd
      inArray.hive += obj.hive
      inArray.order_price.base =
        parseFloat(inArray.order_price.base.split(' ')[0]) + parseFloat(obj.order_price.base.split(' ')[0]) + obj.order_price.base.split(' ')[1]
      inArray.order_price.quote =
        parseFloat(inArray.order_price.quote.split(' ')[0]) + parseFloat(obj.order_price.quote.split(' ')[0]) + obj.order_price.quote.split(' ')[1]
      inArray.real_price = parseFloat(obj.real_price).toFixed(5)
    } else {
      bids.push(JSON.parse(JSON.stringify(obj)))
    }
  })

  return bids
})
export const nonRepeatingBidsReduced = computed(() =>
  nonRepeatingBids.value.map((b, index, array) => {
    const volume = array.reduce((result, obj, i) => (i <= index ? result + obj.hbd / 1000 : result), 0)
    return [parseFloat(parseFloat(b.real_price).toFixed(5)), volume]
  })
)

export const calculateAccountMana = (account: Account) => {
  if (!account || !account.voting_manabar || !account.downvote_manabar) return null

  const effectiveVestingShares = calculateEffectiveVestingShares(account)

  // last_mana + (now - last_update) * max_mana / 5 days
  const maxMana = effectiveVestingShares * 1000000
  const maxDownvoteMana = maxMana / 4
  const nowTimestamp = Date.now() / 1000

  // voting mana
  const elapsed = nowTimestamp - account.voting_manabar.last_update_time
  let currentMana = parseFloat(account.voting_manabar.current_mana) + (elapsed * maxMana) / 432000

  if (currentMana > maxMana) {
    currentMana = maxMana
  }

  // downvote mana
  const downvoteElapsed = nowTimestamp - account.downvote_manabar.last_update_time
  let downvoteCurrentMana = parseFloat(account.downvote_manabar.current_mana) + (downvoteElapsed * maxDownvoteMana) / 432000

  if (downvoteCurrentMana > maxDownvoteMana) {
    downvoteCurrentMana = maxDownvoteMana
  }

  return {
    voting: currentMana / maxMana || 0,
    downvote: downvoteCurrentMana / maxDownvoteMana || 0
  }
}

export const extractOperationAccount = (op: any) => {
  if (!op) return op

  return (
    op.voter || op.author || op.account || op.from || (op.required_auths && op.required_auths[0]) || (op.required_posting_auths && op.required_posting_auths[0])
  )
}

export const setBestNode = (currentBest: string, nodesArr: any[], isHiveEngine: boolean) => {
  const bestNodes = nodesArr
    .filter((node: any) => {
      return isHiveEngine ? nodesArr.filter(node => node.score === 100) : node.features && node.features.includes('get_transaction')
    })
    .filter(node => node.score === 100)
  if (bestNodes.length) {
    const indexOfCurrent = bestNodes.map((node: any) => node.endpoint).indexOf(currentBest)
    if (indexOfCurrent !== -1) {
      return bestNodes[indexOfCurrent]
    }
    const randomIndex = Math.floor(Math.random() * bestNodes.length)
    return bestNodes[randomIndex]
  } else {
    const sortedHeNodes = nodesArr.sort((a: any, b: any) => b.score - a.score)
    return sortedHeNodes[0]
  }
}
