import { RouteRecordRaw } from 'vue-router'

const name = 'polls'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/polls',
    name: 'polls',
    component: () => import('~/polls/pages/Polls.vue'),
    props: true,
    meta: { title: 'Polls' }
  },
  {
    path: '/p/:poll([a-z][a-z0-9-.]{2,15})',
    name: 'poll',
    props: true,
    component: () => import('~/polls/pages/SpecificPoll.vue')
  }
]

export const config = {
  name: name,
  enabled: import.meta.env.VITE_APP_ENABLE_POLLS === 'false' ? false : true, // disabled by default
  routes: routes
}
