/*
THIS DOCUMENT CONTAINS THE DEFINITION FOR AN INTERFACE FOR COMMUNITIES
DEFINITION THAT WILL BE USED IN THE "POLLS" PROJECT
*/

export interface Poll{
    id: string;
    question: string;
    remaining: string;
    creator: string;
    date: string;
    description: string;
    votes: string;
    tags: string [];
    options: string [];


}