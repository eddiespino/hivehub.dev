import axios from 'axios'

const ENDPOINT = import.meta.env.VITE_APP_STATS_ENDPOINT || 'https://polls.hivehub.dev/rpc/polls'

export const fetchPolls = async (): Promise<any> => {
    return (await axios.get(ENDPOINT)).data
  }