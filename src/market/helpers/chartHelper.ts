interface IDataPoint {
  time: number
  date: string
  open: number
  close: number
  high: number
  low: number
  volume?: number
}
/**
 * Find first hive price of trade above roundLimit
 */
const findFirstNormalPrice = (data: any, previousPrice: number = -1, roundLimit: number = 0.075): number => {
  var hivePrice = 0
  for (const trade of data) {
    const hive = trade.current_pays.includes('HIVE') ? parseFloat(trade.current_pays) : parseFloat(trade.open_pays)
    const hbd = trade.current_pays.includes('HBD') ? parseFloat(trade.current_pays) : parseFloat(trade.open_pays)
    hivePrice = hbd / hive
    if (Math.min(hive, hbd) >= roundLimit) {
      return hivePrice
    }
  }
  if(previousPrice > 0) return previousPrice;
  return hivePrice
}

/**
 * Parse data to tradingview chart format with maximum resolution.
 * @param data
 * @param previousPrice default -1 (none)
 * @param roundLimit default 0.075
 * @returns
 */
export const parseChartData = (data: any, previousPrice: number = -1, roundLimit: number = 0.075): Array<IDataPoint> => {
  const parsed: Array<IDataPoint> = []
  let lastRoundedPrice = -1

  for (const trade of data) {
    const timestamp = new Date(trade.date).getTime()
    const date = trade.date
    const hive = trade.current_pays.includes('HIVE') ? parseFloat(trade.current_pays) : parseFloat(trade.open_pays)
    const hbd = trade.current_pays.includes('HBD') ? parseFloat(trade.current_pays) : parseFloat(trade.open_pays)
    let hivePrice = hbd / hive

    if (Math.min(hive, hbd) < roundLimit) {
      if (lastRoundedPrice === -1) {
        lastRoundedPrice = findFirstNormalPrice(data, previousPrice, roundLimit)
      }
      hivePrice = lastRoundedPrice
    } else {
      lastRoundedPrice = hivePrice
    }

    parsed.push({
      time: timestamp,
      date: date,
      open: hivePrice,
      close: hivePrice,
      high: hivePrice,
      low: hivePrice,
      volume: hive /*1*/
    })
  }

  return parsed
}

/**
 * Parse data to tradingview chart format using a specific resolution.
 * @param data
 * @param resolution resolution in minutes
 * @param roundLimit default 0.1
 * @returns
 */
export const parseChartDataResolution = (data: any, resolution: number, roundLimit: number = 0.1): Array<IDataPoint> => {
  const parsed: Array<IDataPoint> = []
  let lastParsed: IDataPoint = { time: 0, open: -1, close: -1, high: -1, low: -1 }
  let lastRoundedPrice = -1
  resolution *= 1000 * 60

  for (const trade of data) {
    const timestamp = new Date(trade.date).getTime()
    const hive = trade.current_pays.includes('HIVE') ? parseFloat(trade.current_pays) : parseFloat(trade.open_pays)
    const hbd = trade.current_pays.includes('HBD') ? parseFloat(trade.current_pays) : parseFloat(trade.open_pays)
    let hivePrice = hbd / hive

    if (Math.min(hive, hbd) < roundLimit) {
      if (lastRoundedPrice === -1) {
        lastRoundedPrice = findFirstNormalPrice(data, roundLimit)
      }
      hivePrice = lastRoundedPrice
    } else {
      lastRoundedPrice = hivePrice
    }

    if (timestamp > lastParsed.time + resolution * 1000 * 60) {
      if (lastParsed.time > 0) {
        parsed.push(lastParsed)
      }
      lastParsed = {
        time: timestamp,
        open: hivePrice,
        close: hivePrice,
        high: hivePrice,
        low: hivePrice
      }
    } else {
      lastParsed.high = Math.max(lastParsed.high, hivePrice)
      lastParsed.low = Math.min(lastParsed.low, hivePrice)
    }
  }

  return parsed
}
