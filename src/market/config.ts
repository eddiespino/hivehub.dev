import { RouteRecordRaw } from 'vue-router'

const name = 'market'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/market/:view?',
    name: 'market',
    component: () => import('~/market/pages/Market.vue'),
    props: true
  },
  {
    path: '/market/limit',
    name: 'limit',
    component: () => import('~/market/pages/Market.vue'),
    props: { default: true, view: 'limit' },
    meta: { expand: true }
  },
  {
    path: '/market/advanced',
    name: 'advanced',
    component: () => import('~/market/pages/Market.vue'),
    props: { default: true, view: 'advanced' },
    meta: { expand: true }
  }
]

export const config = {
  name: name,
  enabled: import.meta.env.VITE_APP_ENABLE_MARKET === 'false' ? false : true, // enabled by default
  routes: routes
}
