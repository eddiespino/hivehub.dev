export const getLocationName = (location: string) => {
  return location.split(' ')[0]
}

export const getLocationType = (location: string) => {
  return location.toLowerCase() === 'token' ? 'chain' : location
}

export const createDepthObjectsArray = (minDepth: number, maxDepth: number) => {
  const depths = []
  for (let depth = minDepth; depth <= maxDepth; depth++) {
    depths.push({ name: depth, value: depth })
  }
  return depths
}

export const findLocationsWithToken = (
  tokenToFind: string,
  currentLocation: string,
  locationsObj: Record<string, { type: string; tokens?: Record<string, boolean>; name: string; icon: string }>
): Array<{ location: string; icon: string; token: string }> => {
  let result: Array<{ location: string; icon: string; token: string }> = []
  tokenToFind = tokenToFind.toUpperCase()
  Object.entries(locationsObj).forEach(([_, location]) => {
    if (location.tokens) {
      Object.keys(location.tokens).forEach(token => {
        if (token.includes(tokenToFind) && location.name !== currentLocation) {
          let entry = {
            location: location.name,
            icon: location.icon,
            token: token
          }
          result.push(entry)
        }
      })
    }
  })

  result.sort((a, b) => {
    if (a.location === b.location) {
      return a.token.localeCompare(b.token)
    }
    return a.location.localeCompare(b.location)
  })

  return result
}

export const filterObjectBySubstring = (originalObject: any, inputValue?: string) => {
  const filteredObject: any = {}
  if (inputValue) {
    const substring = inputValue.toLowerCase()
    Object.keys(originalObject).forEach(key => {
      if (key.includes(substring)) {
        filteredObject[key] = originalObject[key]
      }
    })
  } else {
    Object.keys(originalObject).forEach(key => {
      filteredObject[key] = originalObject[key]
    })
  }
  return filteredObject
}
