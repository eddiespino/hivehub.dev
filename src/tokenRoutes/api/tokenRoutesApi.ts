import axios from 'axios'

const endpoint = 'https://routes.hivehub.dev'

const coingecko = 'https://api.coingecko.com/api/v3'

export const fetchLocationsAndTokens = async (): Promise<any> => {
  return (await axios.get(`${endpoint}/tokens`)).data
}

export const getRoutes = async (md: number, ti: string, li: string, to: string, lo: string, ai: number, mp: number = 10, sl: string = ''): Promise<any> => {
  const routes = (await axios.get(`${endpoint}/route`, { params: { md, ti, li, to, lo, ai, mp, sl } })).data
  return routes
}

export const fetchRoutes = async (
  md: number,
  ti: string,
  li: string,
  to: string,
  lo: string,
  ai: number,
  mp: number = 10,
  sl: string = '',
  fi: any = {}
): Promise<any> => {
  const routes = await axios.post(`${endpoint}/route`, { md, ti, li, to, lo, ai, mp, sl, fi })
  return routes.data
}

export const fetchTokensCoingecko = async (vs_currency: string = 'usd', ids: string = 'hive,bitcoin,ethereum,hive_dollar,cardano,monero'): Promise<any> => {
  return (await axios.get(coingecko + '/coins/markets/', { params: { vs_currency, ids } })).data
}

export const fetchTokenDataCoingecko = async (id: string = 'hive'): Promise<any> => {
  return (await axios.get(`${coingecko}/coins/${id}`)).data
}

export const fetchTokenPrices = async (vs_currency: 'usd', ids: string): Promise<any> => {
  return (await axios.get(coingecko + '/coins/markets/', { params: { vs_currency, ids } })).data
}

export const fetchTopExchanges = async (): Promise<any> => {
  return (await axios.get(coingecko + '/exchanges')).data
}
