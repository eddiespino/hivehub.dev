import { RouteRecordRaw } from 'vue-router'

const name = 'routes'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/routes',
    name: 'routes',
    component: () => import('~/tokenRoutes/pages/Routes.vue'),
    props: true,
    meta: { title: 'Token Routes' }
  },
  {
    path: '/routes/tokens',
    name: 'routes-tokens',
    component: () => import('~/tokenRoutes/pages/Tokens.vue'),
    props: true,
    meta: { title: 'Available Tokens' }
  }
  // {
  //   path: '/routes/locations',
  //   name: 'routes-locations',
  //   component: () => import('~/tokenRoutes/pages/Locations.vue'),
  //   props: true,
  //   meta: { title: 'Available Locations' }
  // },
  // {
  //   path: '/routes/tokens/:token([a-z][a-z0-9-.]{2,15})',
  //   name: 'routes-token-details',
  //   props: true,
  //   meta: { title: 'Token Details' },
  //   component: () => import('~/tokenRoutes/pages/TokenDetails.vue')
  // }
]

export const config = {
  name: name,
  enabled: import.meta.env.VITE_APP_ENABLE_TOKENROUTES === 'false' ? false : true, // disabled by default
  routes: routes
}
