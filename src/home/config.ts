import { RouteRecordRaw } from 'vue-router'

const name = 'home'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: () => '/blocks',
    meta: { title: 'Explorer' }
  },
  {
    path: '/team',
    name: 'team',
    component: () => import('~/home/pages/Team.vue')
  }
]

export const config = {
  name: name,
  enabled: true, // this module cannot be disabled
  routes: routes
}
