import axios from 'axios'

export const getProposalsStats = async (): Promise<any> => {
  return await axios.get('https://stats.hivehub.dev/dhf_proposals')
}

export const getWitnessesStats = async (): Promise<any> => {
  return await axios.get('https://stats.hivehub.dev/witnesses')
}
