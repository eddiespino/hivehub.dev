import axios from 'axios'

const SPLINTERLANDS_ENDPOINT = import.meta.env.VITE_APP_SPLINTERLANDS_ENDPOINT || 'https://api2.splinterlands.com'

export const getTransaction = async (trxId: string): Promise<any> => {
  return (
    await axios.get(`${SPLINTERLANDS_ENDPOINT}/transactions/lookup`, {
      params: {
        trx_id: trxId
      }
    })
  ).data
}
